package com.rohit.commands;

import java.util.ArrayList;
import java.util.List;

import com.rohit.exceptions.PaymentException;

public class Invoker {
	
	   private List<MachineCommand> orderList = new ArrayList<>();
	
	   public void switchOnMachine(){
		   new SwitchOn().execute();
	   }

	   public void dispenseOrder(MachineCommand order){
	      orderList.add(order);	
	   }
	   
	   public void createOrder(Order order) {
		   order.execute();
		}
	   
	   public void  makePayment(PayForOrder pfo) throws PaymentException{
		   pfo.execute();
	   }
	   
	   public void placeOrders(){
	      for (MachineCommand order : orderList) {
	         order.execute();
	      }
	      orderList.clear();
	   }
	   
	   public void switchOffMachine(){
		   new SwitchOff().execute();
	   }

}
