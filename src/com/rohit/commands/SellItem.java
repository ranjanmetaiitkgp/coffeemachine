package com.rohit.commands;

public class SellItem implements MachineCommand{
	
	private MenuItem item;
	
	public SellItem(MenuItem item){
		this.item = item;
	}

	@Override
	public void execute() {
		item.sellItems();
	}

}
