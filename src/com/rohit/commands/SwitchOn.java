package com.rohit.commands;

import java.util.HashMap;
import java.util.Map;

import com.rohit.decorators.Beverage;
import com.rohit.decorators.BeverageChocoDecorator;
import com.rohit.decorators.BeverageCreamDecorator;
import com.rohit.decorators.Coffee;

public class SwitchOn implements MachineCommand{
	
	//package private
	static Map<Integer,Beverage> items = new HashMap<>();

	@Override
	public void execute() {
		items.put(1, new Coffee());
		items.put(2, new BeverageCreamDecorator(new Coffee()));
		items.put(3, new BeverageChocoDecorator(new Coffee()));
		items.put(4, new BeverageChocoDecorator(new BeverageCreamDecorator(new Coffee())));
		System.out.println("Machine started successfully\n");
	}

}
