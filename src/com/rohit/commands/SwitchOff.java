package com.rohit.commands;

public class SwitchOff implements MachineCommand{

	@Override
	public void execute() {
		System.out.println("Machine switched off successfully");
		System.exit(0);
	}

}
