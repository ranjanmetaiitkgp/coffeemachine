package com.rohit.commands;

import java.util.ArrayList;
import java.util.List;

import com.rohit.decorators.Beverage;

public class Order implements MachineCommand{
	
	private List<MenuItem> items = new ArrayList<>();
	private double sum = 0.0;
	
	public Order(List<MenuItem> items){
		this.items = items;
	}

	@Override
	public void execute() {
		System.out.println("Invoice For Order: ");
		for(MenuItem item : items){
			Beverage beverage = SwitchOn.items.get(item.getItemCode());
			sum = sum + (beverage.pricePerQuantity()*item.getQuantity());
			System.out.println("Item [ Code: " + item.getItemCode() +", Quantity: " + item.getQuantity() + ",  Price per quantity " + beverage.pricePerQuantity() + " ]");
		}
		System.out.println("Total Payable Amount : " + sum + "\n");
	}

	public List<MenuItem> getItems() {
		return items;
	}

	public double getSum() {
		return sum;
	}
}
