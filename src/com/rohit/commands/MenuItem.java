package com.rohit.commands;

import com.rohit.decorators.Beverage;
public class MenuItem {
	
	private int itemCode;
	private int quantity;
	

	public int getItemCode() {
		return itemCode;
	}

	public void setItemCode(int itemCode) {
		this.itemCode = itemCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public void sellItems(){
		  Beverage beverage = SwitchOn.items.get(itemCode);
		  //System.out.println("Price per quantity " + beverage.pricePerQuantity());
		  beverage.makeBeverage();
	      System.out.println("Item [ Code: " + itemCode +", Quantity: " + quantity +" ] sold \n");
	}
}
