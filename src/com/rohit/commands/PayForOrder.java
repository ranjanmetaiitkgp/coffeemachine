package com.rohit.commands;

import com.rohit.exceptions.PaymentException;

public class PayForOrder implements MachineCommand{
	
	private Order order;
	private double payAmount;
	private double balance;
	
	public PayForOrder(Order order, double payAmount){
		this.order = order;
		this.payAmount = payAmount;
	}

	@Override
	public void execute() {
		balance = payAmount - order.getSum();
		if(balance < 0){
			throw new PaymentException("Insufficient Payment : Negative net balance ");
		}
		System.out.println("Total Paid Amount : " + payAmount);
		System.out.println("Net Balance : " + balance + "\n");
	}

}
