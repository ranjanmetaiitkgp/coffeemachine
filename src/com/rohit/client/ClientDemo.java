package com.rohit.client;

import java.util.ArrayList;
import java.util.List;
import com.rohit.commands.Invoker;
import com.rohit.commands.MenuItem;
import com.rohit.commands.Order;
import com.rohit.commands.PayForOrder;
import com.rohit.commands.SellItem;
import com.rohit.exceptions.PaymentException;

public class ClientDemo {
	 
	 public static void main(String[] args) {
		 
		  Invoker invoker = new Invoker();
	      invoker.switchOnMachine();
		 
		  List<MenuItem> items = new ArrayList<>();
		 
	      MenuItem item1 = new MenuItem();
	      item1.setItemCode(1);
	      item1.setQuantity(2);
	      items.add(item1);
	      
	      MenuItem item2 = new MenuItem();
	      item2.setItemCode(2);
	      item2.setQuantity(4);
	      items.add(item2);
	      
	      MenuItem item3 = new MenuItem();
	      item3.setItemCode(3);
	      item3.setQuantity(4);
	      items.add(item3);
	      
	      MenuItem item4 = new MenuItem();
	      item4.setItemCode(4);
	      item4.setQuantity(4);
	      items.add(item4);
	      
	      Order order = new Order(items);
	      invoker.createOrder(order);
	      double amount = 900.0;
	      try{
	    	  invoker.makePayment(new PayForOrder(order, amount));
		      invoker.dispenseOrder(new SellItem(item1));
		      invoker.dispenseOrder(new SellItem(item2));
		      invoker.dispenseOrder(new SellItem(item3));
		      invoker.dispenseOrder(new SellItem(item4));
		      invoker.placeOrders();
	      }catch(PaymentException ex){
	    	  System.err.println(ex.getMessage() + (amount - order.getSum()));
	      }finally{
	    	  invoker.switchOffMachine();
	      }
	   }
}
