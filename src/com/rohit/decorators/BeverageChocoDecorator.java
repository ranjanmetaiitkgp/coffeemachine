package com.rohit.decorators;

public class BeverageChocoDecorator extends BeverageDecorator {

	public BeverageChocoDecorator(Beverage decoratedBeverage) {
		super(decoratedBeverage);
	}
	
	@Override
	public void makeBeverage() {
		decoratedBeverage.makeBeverage();	       
        decorateWithCream(decoratedBeverage);
	}

    private void decorateWithCream(Beverage decoratedBeverage){
      System.out.println("Coffee decorated with choco");
    }

    @Override
    public String toString(){
	   return "Coffee decorated with choco";
    }

	@Override
	public double pricePerQuantity() {
		return decoratedBeverage.pricePerQuantity() + 30;
	}

}
