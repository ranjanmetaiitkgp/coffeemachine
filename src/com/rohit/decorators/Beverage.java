package com.rohit.decorators;

public interface Beverage {
	void makeBeverage();
	double pricePerQuantity();
}
