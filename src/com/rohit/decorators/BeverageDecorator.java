package com.rohit.decorators;

public abstract class BeverageDecorator implements Beverage{

	protected Beverage decoratedBeverage;

	public BeverageDecorator(Beverage decoratedBeverage){
      this.decoratedBeverage = decoratedBeverage;
	}

	@Override
	public void makeBeverage(){
	   decoratedBeverage.makeBeverage();
	}	
	
}
