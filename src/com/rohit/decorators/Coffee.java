package com.rohit.decorators;

public class Coffee implements Beverage{

	@Override
	public void makeBeverage() {
		System.out.println("Prepairing Coffee");
	}

	@Override
	public String toString(){
		return "Plain Coffee";
	}

	@Override
	public double pricePerQuantity() {
		return 25;
	}
}
