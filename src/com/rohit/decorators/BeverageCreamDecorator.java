package com.rohit.decorators;

public class BeverageCreamDecorator extends BeverageDecorator{

	public BeverageCreamDecorator(Beverage decoratedBeverage) {
		super(decoratedBeverage);
	}
	
	@Override
	public void makeBeverage() {
		decoratedBeverage.makeBeverage();	       
        decorateWithCream(decoratedBeverage);
	}

   private void decorateWithCream(Beverage decoratedBeverage){
      System.out.println("Coffee decorated with cream");
   }

   @Override
   public String toString(){
	   return "Coffee decorated with cream";
   }

   @Override
   public double pricePerQuantity() {
	   return decoratedBeverage.pricePerQuantity() + 20;
   }
}
