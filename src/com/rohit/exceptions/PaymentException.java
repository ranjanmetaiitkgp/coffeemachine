package com.rohit.exceptions;

public class PaymentException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -197972764028875616L;
	
	public PaymentException(String msg){
		super(msg);
	}

}
